﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class Company
    {
        [Key]
        public string NIT { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Phone { get; set; }
    }
}