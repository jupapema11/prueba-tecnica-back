﻿using Microsoft.AspNetCore.Mvc;
using System;
using WebApplication2.Data;
using WebApplication2.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;

namespace WebApplication2.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly MyDbContext _context;
        private readonly IConfiguration _config;

        public CompanyController(MyDbContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        [HttpGet("")]
        //[Authorize] // Requiere autenticación para acceder al método
        public IActionResult GetCompanies()
        {
            
            try
            {
                // Si la validación es exitosa, devolver los datos solicitados
                var companies = _context.Companies.ToList();
                return Ok(companies);
            }
            catch (Exception ex)
            {
                // Si ocurre un error en la validación, devolver un error 401 (No autorizado)
                return Unauthorized("Operacion no autorizada");
            }
        }



        [HttpPost]
        //[Authorize(Roles = "True")]
        public ActionResult<Company> CreateCompany(Company company)
        {
            // Obtener el token del encabezado de autorización
            var tokenHeader = Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrEmpty(tokenHeader) || !tokenHeader.StartsWith("Bearer "))
            {
                return Unauthorized("Token no encontrado en la solicitud");
            }

            var token = tokenHeader.Split(' ')[1];

            // Crear el token handler y validar el token
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtSettings = _config.GetSection("JwtSettings");
            var jwtKey = jwtSettings.GetValue<string>("JwtKey");
            if (string.IsNullOrEmpty(jwtKey))
            {
                throw new ArgumentException("JwtKey is null or empty.");
            }
            try
            {
                var claimsPrincipal = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.GetValue<string>("JwtKey"))),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);
                var isAdmin = claimsPrincipal.HasClaim(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role" && c.Value == "True");
                if (!isAdmin)
                {
                    return Unauthorized("Operacion no autorizada");
                }

                if (NitExists(company.NIT))
                {
                    return BadRequest("El NIT ya está en uso.");
                }
                _context.Companies.Add(company);
                _context.SaveChanges();
                return Ok(company);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{nit}")]
        public IActionResult EditCompany(string nit, Company company)
        {

            try
            {
                // Obtener el token del encabezado de autorización
                var tokenHeader = Request.Headers["Authorization"].FirstOrDefault();
                if (string.IsNullOrEmpty(tokenHeader) || !tokenHeader.StartsWith("Bearer "))
                {
                    return Unauthorized("Token no encontrado en la solicitud");
                }

                var token = tokenHeader.Split(' ')[1];

                // Crear el token handler y validar el token
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtSettings = _config.GetSection("JwtSettings");
                var jwtKey = jwtSettings.GetValue<string>("JwtKey");
                if (string.IsNullOrEmpty(jwtKey))
                {
                    throw new ArgumentException("JwtKey is null or empty.");
                }
                var claimsPrincipal = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.GetValue<string>("JwtKey"))),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);
                var isAdmin = claimsPrincipal.HasClaim(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role" && c.Value == "True");
                if (!isAdmin)
                {
                    return Unauthorized("Operacion no autorizada");
                }
                var existingCompany = _context.Companies.FirstOrDefault(c => c.NIT == nit);

                if (existingCompany == null)
                {
                    return NotFound();
                }

                existingCompany.Name = company.Name;
                existingCompany.Address = company.Address;
                existingCompany.Phone = company.Phone;
                _context.SaveChanges();

                return Ok(existingCompany);
            }
            catch (Exception ex)
            {
                return BadRequest("Operacion no autorizada");
            }
        }

        private bool NitExists(string nit)
        {
            return _context.Companies.Any(c => c.NIT == nit);
        }

        [HttpDelete("{nit}")]
        public IActionResult DeleteCompany(string nit)
        {
            try
            {
                // Obtener el token del encabezado de autorización
                var tokenHeader = Request.Headers["Authorization"].FirstOrDefault();
                if (string.IsNullOrEmpty(tokenHeader) || !tokenHeader.StartsWith("Bearer "))
                {
                    return Unauthorized("Token no encontrado en la solicitud");
                }

                var token = tokenHeader.Split(' ')[1];

                // Crear el token handler y validar el token
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtSettings = _config.GetSection("JwtSettings");
                var jwtKey = jwtSettings.GetValue<string>("JwtKey");
                if (string.IsNullOrEmpty(jwtKey))
                {
                    throw new ArgumentException("JwtKey is null or empty.");
                }
                var claimsPrincipal = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.GetValue<string>("JwtKey"))),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);
                var isAdmin = claimsPrincipal.HasClaim(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role" && c.Value == "True");
                if (!isAdmin)
                {
                    return Unauthorized("Operacion no autorizada");
                }

                var companyToDelete = _context.Companies.FirstOrDefault(c => c.NIT == nit);

                if (companyToDelete == null)
                {
                    return NotFound();
                }

                // Validar si existen productos asociados a la compañía
                var products = _context.Products.Where(p => p.CompanyId == int.Parse(nit));
                if (products.Any())
                {
                    return BadRequest("No se puede eliminar la compañía porque tiene productos asociados.");
                }

                _context.Companies.Remove(companyToDelete);
                _context.SaveChanges();

                return Ok("Compañía eliminada correctamente.");
            }
            catch (Exception ex)
            {
                return BadRequest("Error al eliminar la compañía.");
            }
        }


    }
}
