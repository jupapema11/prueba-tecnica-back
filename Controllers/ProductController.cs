﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApplication2.Models;
using WebApplication2.Data;

[Route("api/company/{companyId}/products")]
[ApiController]
public class ProductController : ControllerBase
{
    private readonly MyDbContext _context;
    private readonly IConfiguration _config;

    public ProductController(MyDbContext context, IConfiguration config)
    {
        _context = context;
        _config = config;
    }

    [HttpGet("")]
    public IActionResult GetProducts(string companyId)
    {
        try
        {
            var company = _context.Companies.FirstOrDefault(c => c.NIT == companyId);

            if (company == null)
            {
                return NotFound("Compañía no encontrada");
            }
            var products = _context.Products.Where(p => p.CompanyId == int.Parse(companyId)).ToList();
            return Ok(products);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("{id}")]
    public IActionResult GetProduct(int companyId, int id)
    {
        try
        {
            var product = _context.Products.FirstOrDefault(p => p.CompanyId == companyId && p.Id == id);
            if (product == null)
            {
                return NotFound("Producto no encontrado");
            }
            return Ok(product);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost("")]
    public IActionResult CreateProduct(string companyId, [FromBody] ProductDto productDto)
    {
        try
        {
            var company = _context.Companies.FirstOrDefault(c => c.NIT == companyId);
            if (company == null)
            {
                return NotFound("Compañía no encontrada");
            }

            if (_context.Products.Any(p => p.CompanyId == int.Parse(companyId) && p.Name == productDto.Name))
            {
                return BadRequest("Ya existe un producto con ese nombre");
            }

            var product = new Product
            {
                CompanyId = int.Parse(companyId),
                Name = productDto.Name,
                Price = productDto.Price
            };

            _context.Products.Add(product);
            _context.SaveChanges();
            return Ok(product);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }


    public class ProductDto
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }


    [HttpPut("{id}")]
    public IActionResult UpdateProduct(int companyId, int id, [FromBody] Product product)
    {
        try
        {
            var existingProduct = _context.Products.FirstOrDefault(p => p.CompanyId == companyId && p.Id == id);
            if (existingProduct == null)
            {
                return NotFound("Producto no encontrado");
            }

            existingProduct.Name = product.Name;
            existingProduct.Price = product.Price;
            _context.SaveChanges();
            return Ok(existingProduct);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteProduct(int companyId, int id)
    {
        try
        {
            var product = _context.Products.FirstOrDefault(p => p.CompanyId == companyId && p.Id == id);
            if (product == null)
            {
                return NotFound("Producto no encontrado");
            }
            _context.Products.Remove(product);
            _context.SaveChanges();
            return Ok(product);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }
}
