﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    [ApiController]
    [Route("api")]
    public class HomeController : Controller
    {
        // GET: api/Home
        [HttpGet("Home")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<string[]>))]
        public IActionResult Details()
        {
            return Ok(new String[] { "value1" });
        }
    }
}
