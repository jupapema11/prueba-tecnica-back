﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApplication2.Migrations
{
    /// <inheritdoc />
    public partial class NombredelaMigracionA : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Companies_CompanyNIT",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CompanyNIT",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CompanyNIT",
                table: "Products");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CompanyNIT",
                table: "Products",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CompanyNIT",
                table: "Products",
                column: "CompanyNIT");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Companies_CompanyNIT",
                table: "Products",
                column: "CompanyNIT",
                principalTable: "Companies",
                principalColumn: "NIT");
        }
    }
}
